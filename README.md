# PayrollPanda test

These are the instructions to set up the development environment, or to build (from scratch) a new version of the site.

## Cloning the code

In order to clone the code use the following command:

```shell
$ git clone git@bitbucket.org:leondomingo/payrollpanda-test.git
```

## Setting up the environment

Inside the root folder (_payrollpanda-test/_ folder) run the following command. **NodeJS/npm** have to be installed in order to make it work:

```shell
$ npm i
```

## development

To watch for changes on **JS** (_src/js/pp.js_) and **SCSS** (_src/css/style.scss_) files, and transpile them the corresponding sourcemapped files, you have to run the following command inside the root folder (_payrollpanda-test/_ folder):

```shell
$ gulp
```

## build

Once you finish the **development** stage, you run the following command inside the root folder in order to generate the not-sourcemapped-and-minified **JS** and **CSS** files into the folders _js/_ and _css/_ respectively. Old **.map** files are also removed.

```shell
$ gulp build
```
