'use strict'

const gulp         = require('gulp'),
      sass         = require('gulp-sass'),
      minifyCss    = require('gulp-cssnano'),
      autoprefixer = require('gulp-autoprefixer'),
      sourcemaps   = require('gulp-sourcemaps'),
      rename       = require('gulp-rename'),
      newer        = require('gulp-newer'),
      combiner     = require('stream-combiner2'),
      watchify     = require('watchify'),
      browserify   = require('browserify'),
      babelify     = require('babelify'),
      vueify       = require('vueify'),
      aliasify     = require('aliasify'),
      vinylBuffer  = require('vinyl-buffer'),
      source       = require('vinyl-source-stream'),
      notify       = require('gulp-notify'),
      notifier     = require('node-notifier'),
      del          = require('del'),
      replace      = require('replace'),
      path         = require('path')

const PROJECT_TITLE = 'PayrollPanda Test'
const BASE_PATH = '.'
const PATHS = [
  './index.html',
]

const JS_PATHS = [
  './src/js',
  './src/js/components',
]

// JS (general)
var DIST_JS = `${BASE_PATH}/js`

const jsVersion = (version) => {

  del([`${DIST_JS}/pp.*`]) // .js, .map, .gz

  replace({
    regex: /pp\.\d{13,}\.js/g,
    replacement: `pp.${version}.js`,
    paths: PATHS,
    silent: true
  })

} // jsVersion

gulp.task('js:build', function () {

  process.env.NODE_ENV = 'production'

  const version = Date.now()
  jsVersion(version)

  const combined = combiner.obj([
    browserify(`${BASE_PATH}/src/js/pp.js`, {
      paths: JS_PATHS,
    })
      .transform(babelify)
      .transform(vueify)
      .transform(aliasify, {
        aliases: {
          'vue': 'vue/dist/vue.min.js',
        },
        verbose: false,
      })
      .plugin('tinyify', { flat: false })
      .bundle(),
    source(`pp.${version}.js`),
    vinylBuffer(),
    gulp.dest(DIST_JS)
  ])

  combined.on('error', err => {
    if (err.codeFrame) {
      console.error(err.codeFrame)
    }
    else {
      console.error(err.toString())
    }
  })

  combined.on('error', notify.onError({
    title: PROJECT_TITLE,
    message: "<%= error.filename %> (line=<%= error.loc.line %>, column=<%= error.loc.column %>)"
  }))

  return combined

}) // js:build


// Sass (SCSS -> CSS -> CSS min)
const DIST_CSS = `${BASE_PATH}/css`
const sassVersion = (version) => {

  del([
    `${DIST_CSS}/style.*`,
    `${DIST_CSS}/**/*.gz`,
  ])

  // replace
  replace({
    regex: /style\.\d{13,}\.css/g,
    replacement: `style.${version}.css`,
    paths: PATHS,
    silent: true
  })

} // sassVersion

gulp.task('sass:dev', function() {

  const version = Date.now()
  sassVersion(version)

  return gulp.src(`${BASE_PATH}/src/css/style.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error',notify.onError({
      title: PROJECT_TITLE,
      message: "Error: <%= error.message %>"
    })))
    .pipe(rename(`style.${version}.css`))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(DIST_CSS))

}) // sass:dev

gulp.task('sass:build', function() {

  const version = Date.now()
  sassVersion(version)

  return gulp.src(`${BASE_PATH}/src/css/style.scss`)
    .pipe(sass().on('error',notify.onError({
      title: PROJECT_TITLE,
      message: "Error: <%= error.message %>"
    })))
    .pipe(autoprefixer())
    .pipe(minifyCss({ zindex: false }))
    .pipe(rename(`style.${version}.css`))
    .pipe(gulp.dest(DIST_CSS))

}) // sass:build

// CSS
gulp.task('css', function() {
  return gulp.src('./src/css/*.css')
    .pipe(newer(DIST_CSS))
    .pipe(autoprefixer())
    .pipe(minifyCss({
      keepSpecialComments: false,
      zindex: false,
    }))
    .pipe(gulp.dest(DIST_CSS))
})

// Watch Files For Changes
gulp.task('watch', function() {
  const bundler  = watchify(browserify(`${BASE_PATH}/src/js/pp.js`, {
      cache: {},
      packageCache: {},
      fullPaths: true,
      debug: true,
      paths: JS_PATHS,
    })
    .transform(babelify)
    .transform(vueify)
    .transform(aliasify, {
      aliases: {
        'vue': 'vue/dist/vue.common.js',
      },
      verbose: false,
    })
    )

  function rebundle() {
    const version = Date.now()
    console.log(`* rebundle: ${version}`)

    jsVersion(version)

    const combined = combiner.obj([
      bundler.bundle(),
      source(`pp.${version}.js`),
      vinylBuffer(),
      sourcemaps.init(),
      sourcemaps.write('./'),
      gulp.dest(DIST_JS)
    ])

    combined.on('error', err => {
      let message
      if (err.codeFrame) {
        console.error(err.codeFrame)

        message = `${err.filename} (line=${err.loc.line}, column=${err.loc.column})`
      }
      else {
        console.error(err.toString())
        message = err.toString()
      }

      const icon = path.join(__dirname, `${BASE_PATH}/img/error.png`)

      notifier.notify({
        title: PROJECT_TITLE,
        message,
        icon,
      })
    }) // on::error

    return combined

  } // rebundle

  bundler.on('update', rebundle)

  // run any other gulp.watch tasks
  gulp.watch([`${BASE_PATH}/src/css/**/*.scss`], ['sass:dev'])
  gulp.watch([`${BASE_PATH}/src/css/*.css`], ['css'])

  return rebundle()

}) // watch

function showMessage(message) {
  const icon = path.join(__dirname, `${BASE_PATH}/img/pp-icon.png`)

  notifier.notify({
    title: PROJECT_TITLE,
    message,
    icon,
  })
} // showMessage

// build
gulp.task('build', [
  'js:build',
  'sass:build',
  'css'
], () => showMessage('Build completed!'))

// Default Task
gulp.task('default', [
  'sass:dev',
  'css',
  'watch'
], () => showMessage('gulp running!'))
