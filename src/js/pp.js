import 'babel-polyfill'
import Vue from 'vue'
import moment from 'moment'
import uuidv4 from 'uuid/v4'
import UserRegistration from 'user-registration/user-registration.vue'
import BikersList from 'bikers/bikers.vue'


document.addEventListener('DOMContentLoaded', function() {

  console.log('loaded!')

  const $help_title = document.querySelector('.help .title')
  const $help_text = document.querySelector('.help .text')

  document.querySelectorAll('.switch')
    .forEach(s => s.addEventListener('click', function() {
      // FIXME: make a smooth transition
      $help_text.classList.toggle('closed')
      $help_title.classList.toggle('closed')
    }))

  document.querySelector('a[href="#"]').addEventListener('click', function(e) {
    e.preventDefault()
  })
}) // DOMContentLoaded


const registration_day = moment().format(moment.ISO_8601())

new Vue({
  el: '#app',
  components: { UserRegistration, BikersList, },
  data() {
    return {
      bikers: [{
        id: uuidv4(),
        full_name: 'Sir Isaac Neutron',
        email: 'i.newton@cambridge.uk',
        city: 'Woolsthorpe',
        ride_in_group: 'Always',
        dow: '1234567',
        dow_text: 'Everyday',
        registration_day,
      }, {
        id: uuidv4(),
        full_name: 'Albert Einstein',
        email: 'albert.e@princeton.us',
        city: 'Ulm',
        ride_in_group: 'Sometimes',
        dow: '567',
        dow_text: 'Weekends',
        registration_day,
      },
      {
        id: uuidv4(),
        full_name: 'Richard Feynman',
        email: 'rfeynman@mit.com',
        city: 'New York City',
        ride_in_group: 'Never',
        dow: '12345',
        dow_text: 'Weekdays',
        registration_day,
      },]
    }
  },
  // computed: {},
  // watch: {
  //   selected_currency(v) {
  //     console.log('selected_currency=', v)
  //   },
  // },
  // mounted() {},
  created() {
    console.log('Payroll Panda | Test')
  },
  // methods: {}
})
